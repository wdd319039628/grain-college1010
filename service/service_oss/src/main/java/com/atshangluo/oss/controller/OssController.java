package com.atshangluo.oss.controller;

import com.atshangluo.commonutils.R;
import com.atshangluo.oss.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName OssController.java
 * @Description TODO
 * @createTime 2021年07月26日 12:16:00
 */
@Api(description = "文件上传")

@RestController
@RequestMapping("/ossService/file")
public class OssController {

    @Autowired
    private OssService ossService;

    @ApiOperation(value ="上传头像" )
    @PostMapping("/upload")
    public R upload(MultipartFile file){
        //获取上传文件
        // 获取上传后的文件路径
        String url = ossService.uploadFileAvatar(file);


        return R.ok().data("url",url);
    }

}
