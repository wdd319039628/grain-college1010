package com.atshangluo.staservice.service.impl;


import com.atshangluo.staservice.client.RegisterCount;
import com.atshangluo.staservice.entity.Daily;
import com.atshangluo.staservice.mapper.DailyMapper;
import com.atshangluo.staservice.service.DailyService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-08-02
 */
@Service
public class DailyServiceImpl extends ServiceImpl<DailyMapper, Daily> implements DailyService {

    @Autowired
    private RegisterCount registerCountClient;

    @Override
    public void registerCount(String day) {

        //删除已存在的统计对象

        QueryWrapper<Daily> dayQueryWrapper = new QueryWrapper<>();

        dayQueryWrapper.eq("date_calculated", day);

        baseMapper.delete(dayQueryWrapper);

        // 调用统计注册人数
        Integer count = registerCountClient.registerCount(day);
        Integer loginNum = RandomUtils.nextInt(100, 200);//TODO
        Integer videoViewNum = RandomUtils.nextInt(100, 200);//TODO
        Integer courseNum = RandomUtils.nextInt(100, 200);//TODO

        Daily daily = new Daily();

        daily.setRegisterNum(count);
        daily.setLoginNum(loginNum);
        daily.setVideoViewNum(videoViewNum);
        daily.setCourseNum(courseNum);
        daily.setDateCalculated(day);

        baseMapper.insert(daily);

    }

    @Override
    public Map<String, Object> showTable(String type, String start, String end) {

        QueryWrapper<Daily> wrapper = new QueryWrapper<>();

        wrapper.between("date_calculated",start,end);

        wrapper.select(type,"date_calculated");

        List<Daily> dailies = baseMapper.selectList(wrapper);
        Map<String, Object> map = new HashMap<>();
        List<Integer> dataList = new ArrayList<Integer>();
        List<String> dateList = new ArrayList<String>();
        map.put("dataList", dataList);
        map.put("dateList", dateList);

        for (int i = 0; i < dailies.size(); i++) {
            Daily daily = dailies.get(i);
            dateList.add(daily.getDateCalculated());
            switch (type) {
                case "register_num":
                    dataList.add(daily.getRegisterNum());
                    break;
                case "login_num":
                    dataList.add(daily.getLoginNum());
                    break;
                case "video_view_num":
                    dataList.add(daily.getVideoViewNum());
                    break;
                case "course_num":
                    dataList.add(daily.getCourseNum());
                    break;
                default:
                    break;
            }
        }


        return map;
    }
}
