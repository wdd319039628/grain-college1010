package com.atshangluo.eduservice.service.impl;

import com.atshangluo.eduservice.entity.EduComment;
import com.atshangluo.eduservice.mapper.EduCommentMapper;
import com.atshangluo.eduservice.service.EduCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */
@Service
public class EduCommentServiceImpl extends ServiceImpl<EduCommentMapper, EduComment> implements EduCommentService {

}
