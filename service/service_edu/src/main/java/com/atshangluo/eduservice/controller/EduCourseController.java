package com.atshangluo.eduservice.controller;


import com.atshangluo.commonutils.R;

import com.atshangluo.eduservice.client.VideoClient;
import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.vo.CourseInfoForm;
import com.atshangluo.eduservice.entity.vo.CoursePublish;
import com.atshangluo.eduservice.entity.vo.CourseQuery;
import com.atshangluo.eduservice.service.EduCourseService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
@RestController
@RequestMapping("/eduService/course")

public class EduCourseController {

    @Autowired
    private EduCourseService service;





    @ApiOperation(value = "添加课程")
    @PostMapping("add")
    public R addCourse(@RequestBody CourseInfoForm courseInfo){

        System.out.println(courseInfo);

        String id = service.addCourse(courseInfo);

        return R.ok().data("id",id);
    }


    @GetMapping("/getCourse/{courseId}")
    public R getCourseById(@PathVariable("courseId") String courseId){

        CourseInfoForm courseInfoForm = service.getCourseById(courseId);
        return R.ok().data("courseInfo",courseInfoForm);
    }

    @PostMapping("/updateCourse")
    public R updateCourse(@RequestBody CourseInfoForm courseInfo){

        service.updateCourse(courseInfo);

        return R.ok();
    }

    @GetMapping("/getCoursePublishInfo/{courseId}")
    public R getCoursePublishInfo(@PathVariable("courseId") String courseId){

        CoursePublish coursePublish = service.getCoursePublishInfo(courseId);

        return R.ok().data("courseInfo",coursePublish);

    }

    @PostMapping("/publish/{courseId}")
    public R publish(@PathVariable("courseId") String courseId){
        EduCourse course = new EduCourse();
        course.setId(courseId);
        course.setStatus("Normal");
        service.updateById(course);
        return R.ok();
    }


    @PostMapping("/getList/{page}/{limit}")
    public R getCourseList(@PathVariable("page") long page,
                           @PathVariable("limit") long limit,
                           @RequestBody CourseQuery query){

        Page<EduCourse> param = new Page<>(page,limit);

        service.pageQuery(param,query);

        List<EduCourse> courseList =  param.getRecords();

        long total = param.getTotal();

        return R.ok().data("list",courseList).data("total",total);
    }


    @DeleteMapping("/deleteCourse/{courseId}")
    public R removeCourseById(@PathVariable("courseId") String courseId){

        boolean flag = service.removeCourseById(courseId);

        if (flag) {
         return R.ok();
        }else{
            return R.error();
        }
    }

}

