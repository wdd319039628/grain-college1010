package com.atshangluo.uncenterservice.service.impl;

import com.atshangluo.commonutils.JwtUtils;
import com.atshangluo.commonutils.Md5;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.atshangluo.uncenterservice.entity.UcenterMember;
import com.atshangluo.uncenterservice.entity.vo.LoginVo;
import com.atshangluo.uncenterservice.entity.vo.RegisterVo;
import com.atshangluo.uncenterservice.mapper.UcenterMemberMapper;
import com.atshangluo.uncenterservice.service.UcenterMemberService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-31
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

//    @Autowired        // 注册验证码使用
//    private RedisTemplate<String, String> redisTemplate;

    @Override
    public String login(LoginVo member) {

        String phone = member.getMobile();
        String password = member.getPassword();

        // 判断手机号，密码是否为空
        if (StringUtils.isEmpty(phone)||StringUtils.isEmpty(password)){

            throw new EduException(20001,"登录失败");
        }

        // 判断手机号是否正确
        QueryWrapper<UcenterMember> memberQueryWrapper = new QueryWrapper<>();
        memberQueryWrapper.eq("mobile",phone);
        UcenterMember member1 = baseMapper.selectOne(memberQueryWrapper);
        if (member1 == null){ // 没有这个手机号
            throw new EduException(20001,"手机号不存在");
        }

        // 判断密码
        if (!Md5.encrypt(password).equals(member1.getPassword())){
            throw new EduException(20001,"密码错误");
        }

        // 判断是否被禁用
        if (member1.getIsDisabled()){
            throw new EduException(20001,"账号已被冻结");
        }

        // 生成token

        String token = JwtUtils.getJwtToken(member1.getId(),member1.getNickname());

        return token;
    }

    @Override
    public void register(RegisterVo member) {

        // 获取注册使用的数据
        String phone = member.getMobile();
        String name = member.getNickname();
        String password = member.getPassword();
        String code = member.getCode();

        // 判空
        if (StringUtils.isEmpty(password)
                ||StringUtils.isEmpty(name)
                ||StringUtils.isEmpty(code)
                ||StringUtils.isEmpty(phone)){
            throw new EduException(20001,"参数为空");
        }

        // 判断验证码
//        String mobleCode = redisTemplate.opsForValue().get(phone);
//        if (!code.equals(mobleCode)){
//            throw new  EduException(20001,"验证码错误");
//        }

        // 判断手机号是否已经注册
        Integer count = baseMapper.selectCount(new QueryWrapper<UcenterMember>().eq("mobile", phone));
        if (count.intValue()>0){
            throw new EduException(20001,"该手机号已被注册");
        }

        // 添加信息到数据库
        UcenterMember registerMember = new UcenterMember();
        registerMember.setMobile(member.getMobile());
        registerMember.setNickname(member.getNickname());
        registerMember.setPassword(Md5.encrypt(member.getPassword()));
        registerMember.setIsDisabled(false);
        registerMember.setAvatar("https://edu-1232.oss-cn-chengdu.aliyuncs.com/2021/07/30/71d5c991-2a31-42b3-93c0-e5bab2880f74.png");

        baseMapper.insert(registerMember);


    }

    @Override
    public UcenterMember getByOpenid(String openid) {
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("openid",openid);

        UcenterMember member = baseMapper.selectOne(wrapper);
        return member;
    }

    @Override
    public Integer countRegister(String day) {



        return baseMapper.countRegisterDay(day);
    }
}
