package com.atshangluo.eduservice.service.impl;

import com.atshangluo.eduservice.entity.EduCourseDescription;
import com.atshangluo.eduservice.mapper.EduCourseDescriptionMapper;
import com.atshangluo.eduservice.service.EduCourseDescriptionService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 课程简介 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
@Service
public class EduCourseDescriptionServiceImpl extends ServiceImpl<EduCourseDescriptionMapper, EduCourseDescription> implements EduCourseDescriptionService {

    @Override
    public boolean removeDescriptionByCourseId(String courseId) {
        QueryWrapper<EduCourseDescription> wrapper = new QueryWrapper<>();

        wrapper.eq("id",courseId);

        int count = baseMapper.selectCount(wrapper);

        int delete = baseMapper.delete(wrapper);
        if (count==delete){
            return true;
        }else{
            return false;
        }


    }
}
