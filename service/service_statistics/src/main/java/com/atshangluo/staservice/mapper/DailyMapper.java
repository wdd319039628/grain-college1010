package com.atshangluo.staservice.mapper;

import com.atshangluo.staservice.entity.Daily;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 网站统计日数据 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-08-02
 */
public interface DailyMapper extends BaseMapper<Daily> {

}
