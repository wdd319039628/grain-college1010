package com.atshangluo.eduservice.service.impl;

import com.atshangluo.eduservice.entity.EduChapter;
import com.atshangluo.eduservice.entity.EduVideo;
import com.atshangluo.eduservice.entity.chapter.BigChapter;
import com.atshangluo.eduservice.entity.chapter.SmallChapter;
import com.atshangluo.eduservice.mapper.EduChapterMapper;
import com.atshangluo.eduservice.service.EduChapterService;
import com.atshangluo.eduservice.service.EduVideoService;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
@Service
public class EduChapterServiceImpl extends ServiceImpl<EduChapterMapper, EduChapter> implements EduChapterService {

    @Autowired
    private EduVideoService service;

    @Override
    public List<BigChapter> getChapter(String id) {
        List<BigChapter> chapters = new ArrayList<>();


        // 查询章节
        QueryWrapper<EduChapter> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", id);
        wrapper.orderByAsc("sort", "id");
        List<EduChapter> bigChapters = baseMapper.selectList(wrapper);

        // 查询小结
        QueryWrapper<EduVideo> wrapper1 = new QueryWrapper<>();
        wrapper1.eq("course_id", id);
        wrapper1.orderByAsc("sort", "id");
        List<EduVideo> smallChapter = service.list(wrapper1);

        for (EduChapter bigChapter : bigChapters) {
            BigChapter bigChapter1 = new BigChapter();
            BeanUtils.copyProperties(bigChapter, bigChapter1);
            chapters.add(bigChapter1);

            List<SmallChapter> children = new ArrayList<>();
            for (EduVideo eduVideo : smallChapter) {
                if (eduVideo.getChapterId().equals(bigChapter1.getId())) {

                    SmallChapter smallChapter1 = new SmallChapter();

                    BeanUtils.copyProperties(eduVideo, smallChapter1);
                    smallChapter1.setFree(eduVideo.getIsFree());

                    children.add(smallChapter1);
                }
            }

            bigChapter1.setSmallChapters(children);
        }


        return chapters;
    }

    @Override
    public String addChapter(EduChapter chapter) {
        baseMapper.insert(chapter);

        String id = chapter.getId();
        return id;
    }

    @Override
    public boolean removeChapterById(String chapterId) {

        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();
        wrapper.eq("chapter_id",chapterId);
        int count = service.count(wrapper);
        if (count>0) {

            throw new EduException(20001, "请先删除该章节中小结");
        } else {

            int i = baseMapper.deleteById(chapterId);
            if (i <= 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    @Override
    public boolean removeChapterByCourseId(String courseId) {
        QueryWrapper<EduChapter> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("course_id",courseId);

        int integer = baseMapper.selectCount(queryWrapper);

        int delete = baseMapper.delete(queryWrapper);

        if (integer==delete){
            return true;
        }else{
            return false;
        }


    }


}

