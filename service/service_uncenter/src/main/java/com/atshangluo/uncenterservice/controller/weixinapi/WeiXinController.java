package com.atshangluo.uncenterservice.controller.weixinapi;

import com.atshangluo.commonutils.JwtUtils;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.atshangluo.uncenterservice.entity.UcenterMember;
import com.atshangluo.uncenterservice.service.UcenterMemberService;
import com.atshangluo.uncenterservice.utils.HttpClientUtils;
import com.atshangluo.uncenterservice.utils.WeiXinUtils;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName WeiXinController.java
 * @Description TODO
 * @createTime 2021年07月31日 16:56:00
 */

@Controller
@RequestMapping("/api/ucenter/wx")
public class WeiXinController {

    @Autowired
    private UcenterMemberService memberService;


    @GetMapping("/login")
    public String getWeiXinCode(){


        // 微信开放平台授权baseUrl
        String baseUrl = "https://open.weixin.qq.com/connect/qrconnect" +

                "?appid=%s" +

                "&redirect_uri=%s" +

                "&response_type=code" +

                "&scope=snsapi_login" +

                "&state=%s" +

                "#wechat_redirect";

        // 回调地址
        String redirectUrl = WeiXinUtils.WX_OPEN_REDIRECT_URL; //获取业务服务器重定向地址

        try {

            redirectUrl = URLEncoder.encode(redirectUrl, "UTF-8"); //url编码

        } catch (UnsupportedEncodingException e) {

            throw new EduException(20001,e.getMessage());

        }


        // 防止csrf攻击（跨站请求伪造攻击）

        //String state = UUID.randomUUID().toString().replaceAll("-", "");//一般情况下会使用一个随机数

        String state = "imhelen";//为了让大家能够使用我搭建的外网的微信回调跳转服务器，这里填写你在ngrok的前置域名

        System.out.println("state = " + state);

        // 采用redis等进行缓存state 使用sessionId为key 30分钟后过期，可配置

        //键："wechar-open-state-" + httpServletRequest.getSession().getId()

        //值：satte

        //过期时间：30分钟


        //生成qrcodeUrl

        String qrcodeUrl = String.format(

                baseUrl,

                WeiXinUtils.WX_OPEN_APP_ID,

                 redirectUrl,

                state);


        return "redirect:" + qrcodeUrl;

    }


    @GetMapping("/callback")
    public String callback(String code, String state, HttpSession session){

        //得到授权临时票据code
        //从redis中将state获取出来，和当前传入的state作比较
        //如果一致则放行，如果不一致则抛出异常：非法访问
        //向认证服务器发送请求换取access_token
        String baseAccessTokenUrl = "https://api.weixin.qq.com/sns/oauth2/access_token" +

                "?appid=%s" +

                "&secret=%s" +

                "&code=%s" +

                "&grant_type=authorization_code";

        String accessTokenUrl = String.format(baseAccessTokenUrl,
                WeiXinUtils.WX_OPEN_APP_ID,
                WeiXinUtils.WX_OPEN_APP_SECRET,
                code);

        String result = null;

        try {

            result = HttpClientUtils.get(accessTokenUrl);


        } catch (Exception e) {

            throw new EduException(20001, "获取access_token失败");

        }


        //解析json字符串
        Gson gson = new Gson();

        HashMap map = gson.fromJson(result, HashMap.class);

        String accessToken = (String)map.get("access_token");

        String openid = (String)map.get("openid");

        //查询数据库当前用用户是否曾经使用过微信登录
        UcenterMember member = memberService.getByOpenid(openid);

        if(member == null){



            //访问微信的资源服务器，获取用户信息

            String baseUserInfoUrl = "https://api.weixin.qq.com/sns/userinfo" +

                    "?access_token=%s" +

                    "&openid=%s";

            String userInfoUrl = String.format(baseUserInfoUrl, accessToken, openid);

            String resultUserInfo = null;

            try {

                resultUserInfo = HttpClientUtils.get(userInfoUrl);



            } catch (Exception e) {

                throw new EduException(20001, "获取用户信息失败");

            }

            //解析json

            HashMap<String, Object> mapUserInfo = gson.fromJson(resultUserInfo, HashMap.class);

            String nickname = (String)mapUserInfo.get("nickname");

            String headimgurl = (String)mapUserInfo.get("headimgurl");

            //向数据库中插入一条记录

            member = new UcenterMember();

            member.setNickname(nickname);

            member.setOpenid(openid);

            member.setAvatar(headimgurl);

            memberService.save(member);

        }




        // 生成jwt
        String token = JwtUtils.getJwtToken(member.getId(),member.getNickname());

        return "redirect:http://localhost:3000?token="+token;
    }

}
