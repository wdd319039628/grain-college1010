package com.atshangluo.eduservice.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.entity.EduChapter;
import com.atshangluo.eduservice.entity.chapter.BigChapter;
import com.atshangluo.eduservice.service.EduChapterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */

@RestController
@RequestMapping("/eduService/chapter")
public class EduChapterController {

    @Autowired
    private EduChapterService service;

    @GetMapping("/getChapters/{courseId}")
    public R getChapter(@PathVariable("courseId") String courseId){

        List<BigChapter> chapters = service.getChapter(courseId);

        return R.ok().data("chapters",chapters);
    }


    @PostMapping("/addChapter")
    public R addChapter(@RequestBody EduChapter chapter){

        String id = service.addChapter(chapter);
        return R.ok().data("id",id);
    }

    @GetMapping("/getChapter/{chapterId}")
    public R getChapterById(@PathVariable("chapterId") String chapterId){

        EduChapter chapter = service.getById(chapterId);

        return R.ok().data("chapter",chapter);
    }

    @PostMapping("/updateChapter")
    public R updateChapter(
                           @RequestBody EduChapter chapter){


        service.updateById(chapter);

        return R.ok();
    }

    @GetMapping("/delete/{chapterId}")
    public R deleteChapter(@PathVariable("chapterId") String chapterId){

        boolean flag = service.removeChapterById(chapterId);

        if (flag){
            return R.ok();
        }else{
            return R.error();
        }
    }


}

