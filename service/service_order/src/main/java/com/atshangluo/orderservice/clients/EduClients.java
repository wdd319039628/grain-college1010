package com.atshangluo.orderservice.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName EduClients.java
 * @Description TODO
 * @createTime 2021年08月01日 17:40:00
 */
@FeignClient(value = "service-edu")
@Component

public interface EduClients {


    @PostMapping("/eduService/frontCourse/getCourseForOrder/{courseId}")
    com.atshangluo.commonutils.CourseInfoVo getcourseInfo(@PathVariable("courseId") String courseId);
}
