package com.atshangluo.eduservice.controller.front;

import com.atshangluo.commonutils.JwtUtils;
import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.client.OrderClient;

import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.chapter.BigChapter;
import com.atshangluo.eduservice.entity.vo.CourseInfoVo;
import com.atshangluo.eduservice.entity.vo.CourseQueryVo;
import com.atshangluo.eduservice.service.EduChapterService;
import com.atshangluo.eduservice.service.EduCourseService;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName CourseFrontController.java
 * @Description TODO
 * @createTime 2021年08月01日 10:15:00
 */

@RestController
@RequestMapping("/eduService/frontCourse")
public class CourseFrontController {


    @Autowired
    private EduCourseService courseService;

    @Autowired
    private EduChapterService chapterService;

    @Autowired
    private OrderClient orderClient;

    @PostMapping("/getCourse/{page}/{limit}")
    public R getFrontCourse(@PathVariable("page") long page,
                            @PathVariable("limit")long limit,
                            @RequestBody(required = false)CourseQueryVo queryVo){

        Page<EduCourse> param = new Page<>(page,limit);
        HashMap<String,Object> map = courseService.getCourseFront(param,queryVo);

        return R.ok().data(map);
    }



    @GetMapping("/getCourseInfo/{courseId}")
    public R getCourseInfo(@PathVariable("courseId") String courseId, HttpServletRequest request){

        //查询课程信息和讲师信息

        CourseInfoVo courseWebVo = courseService.selectInfoWebById(courseId);

        if (StringUtils.isEmpty(JwtUtils.getMemberIdByJwtToken(request))||StringUtils.isEmpty(courseId)){

            throw new EduException(20001,"参数为空");

        }
        boolean flag = orderClient.isBuyCourse(JwtUtils.getMemberIdByJwtToken(request), courseId);

        //查询当前课程的章节信息

        List<BigChapter> chapterVoList = chapterService.getChapter(courseId);


        return R.ok().data("course", courseWebVo).data("chapterVoList", chapterVoList).data("isBuy",flag);
    }


    @PostMapping("/getCourseForOrder/{courseId}")
    public com.atshangluo.commonutils.CourseInfoVo getcourseInfo(@PathVariable("courseId") String courseId){

        CourseInfoVo course1 = courseService.selectInfoWebById(courseId);
        com.atshangluo.commonutils.CourseInfoVo course = new com.atshangluo.commonutils.CourseInfoVo();
        BeanUtils.copyProperties(course1,course);


        return course;
    }


    @GetMapping("/test")
    public R test(){


        return R.ok();
    }

}
