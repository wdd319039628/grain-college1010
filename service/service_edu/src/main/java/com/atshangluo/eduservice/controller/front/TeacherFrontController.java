package com.atshangluo.eduservice.controller.front;

import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.EduTeacher;
import com.atshangluo.eduservice.service.EduCourseService;
import com.atshangluo.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName TeacherFrontController.java
 * @Description TODO
 * @createTime 2021年08月01日 08:56:00
 */
@RestController

@RequestMapping("/eduService/teacherF")
public class TeacherFrontController {

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

    @GetMapping("/getList/{page}/{limit}")
    public R getTeacherPage(@PathVariable("page") long page,@PathVariable("limit")long limit){

        Page<EduTeacher> teacherPage = new Page<>(page,limit);
        HashMap<String,Object> map = teacherService.getTeacherList(teacherPage);

        return R.ok().data(map);
    }



    @GetMapping("/getTeacherandCourse/{teacherId}")
    public R getTeacherandCoursebyTeacherid(@PathVariable("teacherId")String teacherId){

        QueryWrapper<EduCourse> courseQueryWrapper = new QueryWrapper<>();
        courseQueryWrapper.eq("teacher_id",teacherId);
        List<EduCourse> courseList = courseService.list(courseQueryWrapper);

        EduTeacher teacher = teacherService.getById(teacherId);

        return R.ok().data("course",courseList).data("teacher",teacher);


    }



}
