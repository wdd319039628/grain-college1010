package com.atshangluo.eduservice.controller.front;

import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.EduTeacher;
import com.atshangluo.eduservice.service.EduCourseService;
import com.atshangluo.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName FrontTeacherController.java
 * @Description TODO
 * @createTime 2021年07月30日 15:50:00
 */
@Api(description = "前台教师显示接口")

@RestController
@RequestMapping("/eduService/frontTeacher")
public class IndexFrontController {

    @Autowired
    private EduTeacherService teacherService;

    @Autowired
    private EduCourseService courseService;

   @Cacheable(value = "teacherAndCourse",key = "'index'")
    @GetMapping("/index")
    public R index(){



        QueryWrapper<EduTeacher> teacherQueryWrapper = new QueryWrapper<>();
        teacherQueryWrapper.orderByDesc("id");
        teacherQueryWrapper.last("limit 4");
        List<EduTeacher> teacherList = teacherService.list(teacherQueryWrapper);

        QueryWrapper<EduCourse> courseQueryWrapper = new QueryWrapper<>();

        courseQueryWrapper.orderByDesc("id");
        courseQueryWrapper.last("limit 8");
        List<EduCourse> courseList = courseService.list(courseQueryWrapper);


        return R.ok().data("course",courseList).data("teacher",teacherList);



    }


}
