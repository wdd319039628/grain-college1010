package com.atshangluo.uncenterservice.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName LoginVo.java
 * @Description TODO
 * @createTime 2021年07月31日 10:55:00
 */
@Data
public class LoginVo {

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "密码")
    private String password;
}
