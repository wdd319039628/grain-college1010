package com.atshangluo.eduservice.client;

import com.atshangluo.commonutils.Member;
import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.client.impl.VideoClientImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName VedioClient.java
 * @Description TODO
 * @createTime 2021年07月29日 18:13:00
 */
@FeignClient(value = "service-vod",fallback = VideoClientImpl.class)
@Component
public interface VideoClient {


    @DeleteMapping("/eduVideo/delete/{videoId}")
     R deleteVideo(@PathVariable("videoId") String videoId);

    @DeleteMapping("/eduVideo/deleteByCourse")
     R deleteByCourse(@RequestParam("videoIds") List<String> videoIds);





}
