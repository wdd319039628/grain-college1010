package com.atshangluo.staservice.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName RegisterCount.java
 * @Description TODO
 * @createTime 2021年08月02日 14:29:00
 */
@FeignClient(name = "service-ucenter")
@Component
public interface RegisterCount {


    @GetMapping("/api/member/register/{day}")
    public int registerCount(@PathVariable("day") String day);




}
