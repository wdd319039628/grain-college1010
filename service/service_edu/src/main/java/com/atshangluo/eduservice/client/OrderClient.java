package com.atshangluo.eduservice.client;

import feign.Param;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName OrderIsBuyClient.java
 * @Description TODO
 * @createTime 2021年08月02日 09:40:00
 */
@FeignClient(name = "serviceOrder",path ="/orderService/order")
@Component
public interface OrderClient {

        //查询订单信息

    @GetMapping(value ="/isBuyCourse/{memberid}/{courseid}")
    public boolean isBuyCourse(@PathVariable("memberid") String memberid, @PathVariable("courseid") String id);

}
