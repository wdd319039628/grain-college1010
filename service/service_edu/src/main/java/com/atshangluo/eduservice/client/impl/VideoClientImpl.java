package com.atshangluo.eduservice.client.impl;

import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.client.VideoClient;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName VideoClientImpl.java
 * @Description TODO
 * @createTime 2021年07月30日 10:19:00
 */
@Component
public class VideoClientImpl implements VideoClient {
    @Override
    public R deleteVideo(String videoId) {
        return R.error().message("time out");
    }

    @Override
    public R deleteByCourse(List<String> videoIds) {
        return R.error().message("time out");
    }
}
