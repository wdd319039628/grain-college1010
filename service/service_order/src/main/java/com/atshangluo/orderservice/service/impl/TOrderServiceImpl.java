package com.atshangluo.orderservice.service.impl;

import com.atshangluo.commonutils.CourseInfoVo;
import com.atshangluo.commonutils.Member;
import com.atshangluo.commonutils.OrderNoUtil;
import com.atshangluo.orderservice.clients.EduClients;
import com.atshangluo.orderservice.clients.UserClients;
import com.atshangluo.orderservice.entity.TOrder;
import com.atshangluo.orderservice.mapper.TOrderMapper;
import com.atshangluo.orderservice.service.TOrderService;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */
@Service
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TOrderService {

    @Autowired
    private EduClients eduClients;

    @Autowired
    private UserClients userClients;


    @Override
    public String saveOrder(String courseId, String id) {


        if (StringUtils.isEmpty(id)||StringUtils.isEmpty(courseId)){

            throw new EduException(20001,"参数为空");
        }
       Member member = userClients.getInfo(id);

        CourseInfoVo courseInfoVo = eduClients.getcourseInfo(courseId);

        TOrder order = new TOrder();

        //创建订单
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseId);
        order.setCourseTitle(courseInfoVo.getTitle());
        order.setCourseCover(courseInfoVo.getCover());
        order.setTeacherName(courseInfoVo.getTeacherName());
        order.setTotalFee(courseInfoVo.getPrice());
        order.setMemberId(id);
        order.setMobile(member.getMobile());
        order.setNickname(member.getNickname());
        order.setStatus(0);
        order.setPayType(1);
        baseMapper.insert(order);

        return order.getOrderNo();
    }
}
