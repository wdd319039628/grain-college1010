package com.atshangluo.eduservice.entity.vo;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName SubjectNode.java
 * @Description TODO
 * @createTime 2021年07月27日 10:32:00
 */
public class SubjectNode {

    private String id;
    private String title;

    public SubjectNode() {
    }

    public SubjectNode(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "SubjectNode{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
