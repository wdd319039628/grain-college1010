package com.atshangluo.eduservice.service.impl;

import com.atshangluo.eduservice.entity.EduTeacher;
import com.atshangluo.eduservice.mapper.EduTeacherMapper;
import com.atshangluo.eduservice.service.EduTeacherService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * <p>
 * 讲师 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-24
 */
@Service
public class EduTeacherServiceImpl extends ServiceImpl<EduTeacherMapper, EduTeacher> implements EduTeacherService {

    @Override
    public HashMap<String, Object> getTeacherList(Page<EduTeacher> teacherPage) {
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        baseMapper.selectPage(teacherPage,wrapper);

        HashMap<String,Object> map = new HashMap<>();

        map.put("total",teacherPage.getTotal());
        map.put("list",teacherPage.getRecords());
        map.put("current",teacherPage.getCurrent());
        map.put("size",teacherPage.getSize());
        map.put("hasNext",teacherPage.hasNext());
        map.put("hasPrevious",teacherPage.hasPrevious());
        map.put("pages",teacherPage.getPages());



        return map;
    }
}
