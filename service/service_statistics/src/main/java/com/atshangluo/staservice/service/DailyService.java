package com.atshangluo.staservice.service;

import com.atshangluo.staservice.entity.Daily;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-08-02
 */
public interface DailyService extends IService<Daily> {

    void registerCount(String day);

    Map<String, Object> showTable(String type, String start, String end);
}
