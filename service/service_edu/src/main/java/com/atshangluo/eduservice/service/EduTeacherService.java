package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
 * <p>
 * 讲师 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-24
 */
public interface EduTeacherService extends IService<EduTeacher> {

    HashMap<String, Object> getTeacherList(Page<EduTeacher> teacherPage);
}
