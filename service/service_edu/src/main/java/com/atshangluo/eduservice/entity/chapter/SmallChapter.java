package com.atshangluo.eduservice.entity.chapter;

import lombok.Data;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName SmallChapter.java
 * @Description TODO
 * @createTime 2021年07月28日 09:20:00
 */
@Data
public class SmallChapter {

    private String id;

    private String title;

    private boolean free;

    private String videoSourceId;

}
