package com.atshangluo.eduservice.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName CourseQuery.java
 * @Description TODO
 * @createTime 2021年07月29日 09:50:00
 */
@Data
public class CourseQuery implements Serializable {

    private static final long serialVersionUID = 1L;

    private String subjectId;

    private String subjectParentId;

    private String teacherId;

    private String title;
}
