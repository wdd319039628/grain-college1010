package com.atshangluo.eduservice.controller;

import com.atshangluo.commonutils.R;
import org.springframework.web.bind.annotation.*;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName EduLoginController.java
 * @Description TODO
 * @createTime 2021年07月25日 16:17:00
 */

@RestController
@RequestMapping("/eduService/user")
public class EduLoginController {

    @PostMapping("/login")
    public R login(){

       return R.ok().data("token","admin");

    }


    @GetMapping("/info")
    public R info(){

      return R.ok().data("roles","admin").data("name","admin").data("avatar","https://dfzximg01.dftoutiao.com/news/20210725/20210725092518_ef537fd913266859c1d1a93552450851_1.jpeg");
    }
}
