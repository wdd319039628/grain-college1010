package com.atshangluo.video.controller;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthRequest;
import com.aliyuncs.vod.model.v20170321.GetVideoPlayAuthResponse;
import com.atshangluo.commonutils.R;
import com.atshangluo.video.service.VideoService;
import com.atshangluo.video.utils.ClientUtils;
import com.atshangluo.video.utils.PropertiesUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName VideoController.java
 * @Description TODO
 * @createTime 2021年07月29日 14:56:00
 */

@RequestMapping("/eduVideo")
@RestController
public class VideoController {

    @Autowired
    private VideoService videoService;

    @PostMapping("/upload/video")
    public R uploadVideo(@RequestParam("video") MultipartFile video){



        String info[] = videoService.uploadVideo(video);

        if ("404".equals(info[2])){
            return R.error();
        }else{
            return R.ok().data("videoId",info[1]).data("name",info[0]);

        }

    }

    @DeleteMapping("/delete/{videoId}")
    public R deleteVideo(@PathVariable("videoId") String videoId){

        videoService.deleteVideo(videoId);

        return R.ok().message("删除成功");
    }



    // 删除多个视频

    @DeleteMapping("/deleteByCourse")
    public R deleteByCourse(@RequestParam("videoIds")List<String> videoIds){

         videoService.deleteVideoByCourse(videoIds);

        return R.ok().message("删除成功");
    }



    @GetMapping("/getPlayAuth/{videoId}")
    public R getPlayAuth(@PathVariable("videoId")String videoId){

        try{

            //获取阿里云存储相关常量
            String accessKeyId = PropertiesUtils.ACCESS_KEY_ID;
            String accessKeySecret = PropertiesUtils.ACCESS_KEY_SECRET;

            // 初始化
            DefaultAcsClient client = ClientUtils.initVodClient(accessKeyId,accessKeySecret);

            // 请求
            GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
            request.setVideoId(videoId);

            // 响应
            GetVideoPlayAuthResponse response = client.getAcsResponse(request);

            // 得到播放凭证
            String playAuth = response.getPlayAuth();

            return R.ok().data("playAuth",playAuth);


        }catch (Exception e){

             return R.ok().message("服务器错误");
        }



    }





}
