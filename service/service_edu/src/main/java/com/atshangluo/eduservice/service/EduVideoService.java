package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
public interface EduVideoService extends IService<EduVideo> {

    boolean removeVideoById(String courseId);



}
