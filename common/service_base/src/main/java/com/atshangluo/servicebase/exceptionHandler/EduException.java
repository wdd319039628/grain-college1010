package com.atshangluo.servicebase.exceptionHandler;

import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName EduException.java
 * @Description TODO
 * @createTime 2021年07月24日 16:18:00
 */

public class EduException extends RuntimeException{
    @ApiParam(name = "code",value = "状态码")
    private Integer code;
    @ApiParam(name = "msg",value = "错误信息")
    private String msg;

    public EduException(Integer code,String msg){
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
