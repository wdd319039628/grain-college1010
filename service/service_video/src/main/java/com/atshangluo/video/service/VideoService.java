package com.atshangluo.video.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName VideoService.java
 * @Description TODO
 * @createTime 2021年07月29日 14:55:00
 */
public interface VideoService {

    String[] uploadVideo(MultipartFile video);

    void deleteVideo(String videoId);

    void deleteVideoByCourse(List<String> videoIds);
}
