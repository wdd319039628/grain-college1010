package com.atshangluo.acl.service.impl;

import com.atshangluo.acl.entity.RolePermission;
import com.atshangluo.acl.mapper.RolePermissionMapper;
import com.atshangluo.acl.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色权限 服务实现类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}
