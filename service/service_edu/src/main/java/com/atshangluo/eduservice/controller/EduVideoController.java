package com.atshangluo.eduservice.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.client.VideoClient;
import com.atshangluo.eduservice.entity.EduVideo;
import com.atshangluo.eduservice.service.EduVideoService;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 课程视频 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */

@RestController
@RequestMapping("/eduService/video")
public class EduVideoController {

    @Autowired
    private EduVideoService service;

    @Autowired
    private VideoClient videoClient;

    @PostMapping("/add")
    public R addVideo(@RequestBody EduVideo video){

        service.save(video);
        return R.ok();
    }

    @ApiOperation(value = "删除小结以及视频")
    @DeleteMapping("/delete/{id}")
    public R deleteVideo(@PathVariable("id") String id){
        EduVideo video = service.getById(id);
        String videoId = video.getVideoSourceId();
        if (!StringUtils.isEmpty(videoId)){
           R result =  videoClient.deleteVideo(videoId);
           if (result.getCode()==20001) {
               throw new EduException(20001,"删除视频失败");
           }
        }

        service.removeById(id);
        return R.ok();
    }

    @GetMapping("/get/{id}")
    public R getVideo(@PathVariable("id") String id){

        EduVideo video = service.getById(id);
        return R.ok().data("video",video);
    }

    @PostMapping("/update")
    public R update(@RequestBody EduVideo video){

        boolean flag = service.updateById(video);
        if (flag){
            return R.ok();
        }else{

            return R.error();
        }
    }


}

