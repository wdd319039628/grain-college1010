package com.atshangluo.eduservice.entity.vo;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName ExcelSubjectData.java
 * @Description TODO
 * @createTime 2021年07月26日 17:56:00
 */
public class ExcelSubjectData {

    @ExcelProperty(index = 0)
    private String oneSubjectName;

    @ExcelProperty(index = 1)
    private String twoSubjectName;

    public ExcelSubjectData() {
    }

    public ExcelSubjectData(String oneSubjectName, String twoSubjectName) {
        this.oneSubjectName = oneSubjectName;
        this.twoSubjectName = twoSubjectName;
    }

    public String getOneSubjectName() {
        return oneSubjectName;
    }

    public void setOneSubjectName(String oneSubjectName) {
        this.oneSubjectName = oneSubjectName;
    }

    public String getTwoSubjectName() {
        return twoSubjectName;
    }

    public void setTwoSubjectName(String twoSubjectName) {
        this.twoSubjectName = twoSubjectName;
    }

    @Override
    public String toString() {
        return "ExcelSubjectData{" +
                "oneSubjectName='" + oneSubjectName + '\'' +
                ", twoSubjectName='" + twoSubjectName + '\'' +
                '}';
    }
}
