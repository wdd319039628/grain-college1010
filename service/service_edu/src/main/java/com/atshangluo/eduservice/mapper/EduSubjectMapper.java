package com.atshangluo.eduservice.mapper;

import com.atshangluo.eduservice.entity.EduSubject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-26
 */
@Repository
public interface EduSubjectMapper extends BaseMapper<EduSubject> {

}
