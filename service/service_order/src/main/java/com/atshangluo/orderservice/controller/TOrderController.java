package com.atshangluo.orderservice.controller;


import com.atshangluo.commonutils.JwtUtils;
import com.atshangluo.commonutils.R;
import com.atshangluo.orderservice.entity.TOrder;
import com.atshangluo.orderservice.service.TOrderService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */

@RestController
@RequestMapping("/orderService/order")
public class TOrderController {

    @Autowired
    private TOrderService orderService;

    @PostMapping("/createOrder/{courseId}")
    public R createOrder(@PathVariable("courseId")String courseId, HttpServletRequest request){

      String orderId = orderService.saveOrder(courseId, JwtUtils.getMemberIdByJwtToken(request));


      return R.ok().data("orderId",orderId);




    }


    @GetMapping("/get/{orderId}")
    public R getOrder(@PathVariable("orderId") String orderId){

        QueryWrapper<TOrder> wrapper = new QueryWrapper<>();
        wrapper.eq("order_no",orderId);
        TOrder order = orderService.getOne(wrapper);


        return R.ok().data("order",order);
    }




    @GetMapping("/isBuyCourse/{memberid}/{id}")
    public Boolean isBuyCourse(@PathVariable String memberid,
                               @PathVariable String id) {

        //订单状态是1表示支付成功
        int count = orderService.count(new QueryWrapper<TOrder>().eq("member_id", memberid).eq("course_id", id).eq("status", 1));

        if(count>0) {
            return true;
        } else {
            return false;

        }

    }



   }










