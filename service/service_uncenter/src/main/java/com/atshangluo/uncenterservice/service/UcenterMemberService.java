package com.atshangluo.uncenterservice.service;

import com.atshangluo.uncenterservice.entity.UcenterMember;
import com.atshangluo.uncenterservice.entity.vo.LoginVo;
import com.atshangluo.uncenterservice.entity.vo.RegisterVo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-31
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    String login(LoginVo member);

    void register(RegisterVo member);

    UcenterMember getByOpenid(String openid);

    Integer countRegister(String day);
}
