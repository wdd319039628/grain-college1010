package com.atshangluo.eduservice.service.impl;

import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.EduCourseDescription;
import com.atshangluo.eduservice.entity.EduTeacher;
import com.atshangluo.eduservice.entity.vo.*;
import com.atshangluo.eduservice.mapper.EduCourseMapper;
import com.atshangluo.eduservice.service.EduChapterService;
import com.atshangluo.eduservice.service.EduCourseDescriptionService;
import com.atshangluo.eduservice.service.EduCourseService;
import com.atshangluo.eduservice.service.EduVideoService;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.HashMap;

/**
 * <p>
 * 课程 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
@Service
public class EduCourseServiceImpl extends ServiceImpl<EduCourseMapper, EduCourse> implements EduCourseService {

    @Autowired
    private EduCourseDescriptionService descriptionService;
    @Autowired
    private EduVideoService videoService;
    @Autowired
    private EduChapterService chapterService;

    @Override
    public String addCourse(CourseInfoForm info) {

        // 1.向课程表添加信息
        EduCourse eduCourse = new EduCourse();
        BeanUtils.copyProperties(info,eduCourse);
        int i = baseMapper.insert(eduCourse);

        if (i<=0){
            throw new EduException(20001,"添加失败");
        }

        String cId = eduCourse.getId();

        // 2.向课程描述表添加信息
        EduCourseDescription description = new EduCourseDescription();
        BeanUtils.copyProperties(info,description);
        description.setId(cId);
        boolean d = descriptionService.save(description);
        return cId;
    }

    @Override
    public CourseInfoForm getCourseById(String courseId) {

        EduCourse course = baseMapper.selectById(courseId);
        CourseInfoForm courseInfoForm = new CourseInfoForm();
        BeanUtils.copyProperties(course,courseInfoForm);

        EduCourseDescription courseDescription = descriptionService.getById(courseId);
        courseInfoForm.setDescription(courseDescription.getDescription());

        return courseInfoForm;
    }

    @Override
    public void updateCourse(CourseInfoForm courseInfo) {

        EduCourse course = new EduCourse();
        BeanUtils.copyProperties(courseInfo,course);
        int i = baseMapper.updateById(course);

        if (i<=0){
            throw new EduException(20001,"修改失败");
        }

        EduCourseDescription description = new EduCourseDescription();
        description.setId(courseInfo.getId());
        description.setDescription(courseInfo.getDescription());
        descriptionService.updateById(description);
    }

    @Override
    public CoursePublish getCoursePublishInfo(String courseId) {

        CoursePublish coursePublish = baseMapper.selectCoursePublishById(courseId);
        return coursePublish;
    }

    @Override
    public void pageQuery(Page<EduCourse> param, CourseQuery query) {


        QueryWrapper<EduCourse>  wrapper = new QueryWrapper<>();

        if (!StringUtils.isEmpty(query.getTitle())){
            wrapper.like("title",query.getTitle());
        }

        if (!StringUtils.isEmpty(query.getTeacherId())){
            wrapper.eq("teacher_id",query.getTeacherId());
        }

        if (!StringUtils.isEmpty(query.getSubjectId())){
            wrapper.eq("subject_id",query.getSubjectId());
        }

        if (!StringUtils.isEmpty(query.getSubjectParentId())){
            wrapper.eq("subject_parent_id",query.getSubjectParentId());
        }


        baseMapper.selectPage(param,wrapper);
    }

    @Override
    public boolean removeCourseById(String courseId) {

        boolean flagVideo = videoService.removeVideoById(courseId);

        if (!flagVideo){
            throw  new EduException(20001,"小节删除失败");
        }

        boolean flagChapter = chapterService.removeChapterByCourseId(courseId);
        if (!flagChapter){
            throw new EduException(20001,"章节删除失败");
        }

        boolean flagDescription = descriptionService.removeDescriptionByCourseId(courseId);
        if (!flagDescription){
            throw new EduException(20001,"课程描述删除失败");
        }

        QueryWrapper<EduCourse> wrapper = new QueryWrapper<>();
        wrapper.eq("id",courseId);
        int count = baseMapper.selectCount(wrapper);
        int delete = baseMapper.delete(wrapper);
        if (count==delete){
            return true;
        }else{
            throw new EduException(20001,"课程删除失败");
        }

    }

    @Override
    public HashMap<String, Object> getCourseFront(Page<EduCourse> param, CourseQueryVo queryVo) {

        // 根据讲师Id查询所有课程
        QueryWrapper<EduCourse> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(queryVo.getSubjectParentId())) {

            queryWrapper.eq("subject_parent_id", queryVo.getSubjectParentId());

        }


        if (!StringUtils.isEmpty(queryVo.getSubjectId())) {

            queryWrapper.eq("subject_id", queryVo.getSubjectId());

        }


        if (!StringUtils.isEmpty(queryVo.getBuyCountSort())) {

            queryWrapper.orderByDesc("buy_count");

        }


        if (!StringUtils.isEmpty(queryVo.getGmtCreateSort())) {

            queryWrapper.orderByDesc("gmt_create");

        }


        if (!StringUtils.isEmpty(queryVo.getPriceSort())) {

            queryWrapper.orderByDesc("price");

        }

        baseMapper.selectPage(param,queryWrapper);

        HashMap<String,Object> map = new HashMap<>();

        map.put("items", param.getRecords());
        map.put("current", param.getCurrent());
        map.put("pages", param.getPages());
        map.put("size", param.getSize());
        map.put("total", param.getTotal());
        map.put("hasNext", param.hasNext());
        map.put("hasPrevious", param.hasPrevious());




        return map;
    }


    /**

     * 获取课程信息

     * @param id

     * @return

     */
    @Override
    public CourseInfoVo selectInfoWebById(String id) {
        this.updatePageViewCount(id);

        return baseMapper.selectInfoWebById(id);

    }

    /**
     * 更新课程浏览数

     * @param id

     */
    @Override
    public void updatePageViewCount(String id) {

        EduCourse course = baseMapper.selectById(id);

        course.setViewCount(course.getViewCount() + 1);

        baseMapper.updateById(course);

    }
}
