package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
public interface EduCourseDescriptionService extends IService<EduCourseDescription> {

    boolean removeDescriptionByCourseId(String courseId);
}
