package com.atshangluo.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName GateApplication.java
 * @Description TODO
 * @createTime 2021年08月03日 18:23:00
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GateApplication {

    public static void main(String[] args) {

        SpringApplication.run(GateApplication.class,args);

    }
}
