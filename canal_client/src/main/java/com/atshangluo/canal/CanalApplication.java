package com.atshangluo.canal;

import com.atshangluo.canal.client.CanalClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName CanalApplication.java
 * @Description TODO
 * @createTime 2021年08月03日 15:51:00
 */
@SpringBootApplication
public class CanalApplication implements CommandLineRunner {

    @Resource
    private CanalClient canalClient;

    public static void main(String[] args) {
        SpringApplication.run(CanalApplication.class,args);
    }


    @Override
    public void run(String... args) throws Exception {
        canalClient.run();
    }
}
