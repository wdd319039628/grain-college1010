package com.atshangluo.commonutils;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName ResultCode.java
 * @Description TODO
 * @createTime 2021年07月24日 12:18:00
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;

    public static Integer ERROR = 20001;
}
