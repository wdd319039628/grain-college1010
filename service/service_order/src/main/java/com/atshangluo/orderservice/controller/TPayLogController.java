package com.atshangluo.orderservice.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.orderservice.service.TPayLogService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 支付日志表 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */
@RestController

@RequestMapping("/orderService/paylog")
public class TPayLogController {

    @Autowired
    private TPayLogService payLogService;


    @ApiOperation(value = "生成二维码")
    @GetMapping("/createNative/{orderNo}")
    public R createCode(@PathVariable("orderNo") String orderNo){
        Map map = payLogService.createNative(orderNo);

        return R.ok().data(map);
    }



    @GetMapping("/queryPayStatus/{orderNo}")
    public R queryPayStatus(@PathVariable String orderNo) {

        //调用查询接口
        Map<String, String> map = payLogService.queryPayStatus(orderNo);
        if (map == null) {//出错
            return R.error().message("支付出错");
        }
        if (map.get("trade_state").equals("SUCCESS")) {//如果成功
            //更改订单状态
            payLogService.updateOrderStatus(map);

            return R.ok().message("支付成功");

        }
        return R.ok().code(25000).message("支付中");

    }




}

