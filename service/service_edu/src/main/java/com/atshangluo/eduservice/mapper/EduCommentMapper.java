package com.atshangluo.eduservice.mapper;

import com.atshangluo.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */
public interface EduCommentMapper extends BaseMapper<EduComment> {

}
