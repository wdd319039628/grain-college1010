package com.atshangluo.staservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName StateApplication.java
 * @Description TODO
 * @createTime 2021年08月02日 14:12:00
 */
@EnableScheduling
@SpringBootApplication
@MapperScan("com.atshangluo.staservice.mapper")
@ComponentScan("com.atshangluo")
@EnableDiscoveryClient
@EnableFeignClients
public class StateApplication {

    public static void main(String[] args) {
        SpringApplication.run(StateApplication.class,args);
    }

}
