package com.atshangluo.eduservice.entity.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName CoursePublish.java
 * @Description TODO
 * @createTime 2021年07月28日 19:19:00
 */
@Data
public class CoursePublish implements Serializable {

    private static final long serialVersionUID = 1L;

    private String title;

    private String cover;

    private Integer lessonNum;

    private String subjectLevelOne;

    private String subjectLevelTwo;

    private String teacherName;

    private String price;//只用于显示




}
