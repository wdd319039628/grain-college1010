package com.atshangluo.oss.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName OssService.java
 * @Description TODO
 * @createTime 2021年07月26日 12:16:00
 */
public interface OssService {

    String uploadFileAvatar(MultipartFile file);

}
