package com.atshangluo.eduservice;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName EduApplication.java
 * @Description TODO
 * @createTime 2021年07月24日 10:27:00
 */
@EnableFeignClients(basePackages = "com.atshangluo.eduservice.client")
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.atshangluo"})
public class EduApplication {
    public static void main(String[] args) {
        SpringApplication.run(EduApplication.class,args);
    }
}
