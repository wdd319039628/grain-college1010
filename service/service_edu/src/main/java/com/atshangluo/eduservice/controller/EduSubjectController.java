package com.atshangluo.eduservice.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.entity.vo.SubjectNodeTwo;
import com.atshangluo.eduservice.service.EduSubjectService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-07-26
 */
@Api(description = "课程管理")
@RestController
@RequestMapping("/eduService/subject")

public class EduSubjectController {

    @Autowired
    private EduSubjectService service;


    @ApiOperation(value = "添加课程分类")
    @PostMapping("/addSubject")
    public R addSubject(MultipartFile file){

        service.addSubject(file,service);
        return R.ok();
    }


    @ApiOperation(value = "获取课程列表")
    @GetMapping("/getList")
    public R getSubjectList(){

        List<SubjectNodeTwo> list = service.nestedList();


        return R.ok().data("tree",list);
    }



}

