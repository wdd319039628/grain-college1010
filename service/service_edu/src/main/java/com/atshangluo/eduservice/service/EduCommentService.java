package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */
public interface EduCommentService extends IService<EduComment> {

}
