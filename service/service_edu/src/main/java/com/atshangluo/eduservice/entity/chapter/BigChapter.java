package com.atshangluo.eduservice.entity.chapter;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName BigChapter.java
 * @Description TODO
 * @createTime 2021年07月28日 09:20:00
 */
@Data
public class BigChapter {

    private String id;

    private String title;

    private List<SmallChapter>  smallChapters = new ArrayList<>();
}
