package com.atshangluo.eduservice.mapper;

import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.vo.CourseInfoVo;
import com.atshangluo.eduservice.entity.vo.CoursePublish;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
@Repository
public interface EduCourseMapper extends BaseMapper<EduCourse> {

    CoursePublish selectCoursePublishById(String courseId);


    CourseInfoVo selectInfoWebById(String courseId);



}
