package com.atshangluo.eduservice.mapper;

import com.atshangluo.eduservice.entity.EduCourseDescription;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 课程简介 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
public interface EduCourseDescriptionMapper extends BaseMapper<EduCourseDescription> {

}
