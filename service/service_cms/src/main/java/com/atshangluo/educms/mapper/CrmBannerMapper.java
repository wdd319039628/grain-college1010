package com.atshangluo.educms.mapper;

import com.atshangluo.educms.entity.CrmBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 首页banner表 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-30
 */
public interface CrmBannerMapper extends BaseMapper<CrmBanner> {

}
