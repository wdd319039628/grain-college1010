package com.atshangluo.orderservice.clients;

import com.atshangluo.commonutils.Member;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName UserClients.java
 * @Description TODO
 * @createTime 2021年08月01日 17:40:00
 */
@FeignClient(value = "service-ucenter",url = "http://localhost:8160",path ="/api/member" )
@Component
public interface UserClients {

    @GetMapping("/getUserInfo/user/{userId}")
    public com.atshangluo.commonutils.Member getInfo(@PathVariable("userId") String userId);
}
