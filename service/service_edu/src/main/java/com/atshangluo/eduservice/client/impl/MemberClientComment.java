package com.atshangluo.eduservice.client.impl;

import com.atshangluo.commonutils.Member;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.Mapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName MemberClient.java
 * @Description TODO
 * @createTime 2021年08月01日 15:31:00
 */
@FeignClient(value = "service-ucenter")
@Component
public interface MemberClientComment {


    @GetMapping("/api/member/getUserInfo/user/{userId}")
    public com.atshangluo.commonutils.Member getInfo(@PathVariable("userId") String id);
}
