package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduSubject;
import com.atshangluo.eduservice.entity.vo.SubjectNodeTwo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-26
 */

public interface EduSubjectService extends IService<EduSubject> {

    void addSubject(MultipartFile file,EduSubjectService service);

    List<SubjectNodeTwo> nestedList();
}
