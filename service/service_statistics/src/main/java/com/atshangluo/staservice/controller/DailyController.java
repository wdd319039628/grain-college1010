package com.atshangluo.staservice.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.staservice.service.DailyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * <p>
 * 网站统计日数据 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-08-02
 */

@RestController
@RequestMapping("/staService/daily")
public class DailyController {

    @Autowired
    private DailyService  dailyService;

    @GetMapping("/countRegister/{day}")
    public R registerCount(@PathVariable("day") String day){

        dailyService.registerCount(day);

        return R.ok();


    }


    @ApiOperation(value = "图表数据显示")
    @GetMapping("/get/{type}/{start}/{end}")
    public R getTable(@PathVariable("type") String type,
                      @PathVariable("start") String start,
                      @PathVariable("end") String end){

        Map<String,Object> map = dailyService.showTable(type,start,end);

        return R.ok().data(map);

    }


}

