package com.atshangluo.eduservice.service.impl;

import com.atshangluo.eduservice.entity.EduComment;
import com.atshangluo.eduservice.entity.EduCourseDescription;
import com.atshangluo.eduservice.mapper.EduCommentMapper;
import com.atshangluo.eduservice.mapper.EduCourseDescriptionMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName EduCommentService.java
 * @Description TODO
 * @createTime 2021年08月01日 15:42:00
 */
@Service
public class EduCommentService extends ServiceImpl<EduCommentMapper, EduComment> implements com.atshangluo.eduservice.service.EduCommentService {
}
