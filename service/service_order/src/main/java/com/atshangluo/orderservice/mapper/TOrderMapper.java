package com.atshangluo.orderservice.mapper;

import com.atshangluo.orderservice.entity.TOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-08-01
 */
public interface TOrderMapper extends BaseMapper<TOrder> {

}
