package com.atshangluo.educms.controller;

import com.atshangluo.commonutils.R;
import com.atshangluo.educms.entity.CrmBanner;
import com.atshangluo.educms.service.CrmBannerService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName FrontBannerController.java
 * @Description TODO
 * @createTime 2021年07月30日 15:21:00
 */
@Api(description = "前台banner显示接口")

@RestController
@RequestMapping("/eduCms/frontBanner")
public class FrontBannerController {

    @Autowired
    private CrmBannerService bannerService;


    @Cacheable(value ="banner" ,key ="'selectIndexBanner'")
    @GetMapping("/query")
    public R getBanner(){

        List<CrmBanner> bannerList = bannerService.getAllBanner();

        return R.ok().data("list",bannerList);
    }

}
