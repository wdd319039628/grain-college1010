package com.atshangluo.commonutils;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName Member.java
 * @Description TODO
 * @createTime 2021年08月01日 15:26:00
 */
@Data
public class Member {


    private String nickname;

    private Integer sex;

    private String avatar;


    @ApiModelProperty(value = "会员id")

    private String id;

    @ApiModelProperty(value = "微信openid")
    private String openid;

    @ApiModelProperty(value = "手机号")
    private String mobile;

}
