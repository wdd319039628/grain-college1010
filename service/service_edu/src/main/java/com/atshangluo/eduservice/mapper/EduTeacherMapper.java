package com.atshangluo.eduservice.mapper;

import com.atshangluo.eduservice.entity.EduTeacher;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 讲师 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-24
 */
@Repository
public interface EduTeacherMapper extends BaseMapper<EduTeacher> {

}
