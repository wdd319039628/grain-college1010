package com.atshangluo.eduservice.service.impl;

import com.alibaba.excel.EasyExcel;
import com.atshangluo.eduservice.entity.EduSubject;
import com.atshangluo.eduservice.entity.vo.ExcelSubjectData;
import com.atshangluo.eduservice.entity.vo.SubjectNode;
import com.atshangluo.eduservice.entity.vo.SubjectNodeTwo;
import com.atshangluo.eduservice.listener.SubjectListen;
import com.atshangluo.eduservice.mapper.EduSubjectMapper;
import com.atshangluo.eduservice.service.EduSubjectService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程科目 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-26
 */
@Service
public class EduSubjectServiceImpl extends ServiceImpl<EduSubjectMapper, EduSubject> implements EduSubjectService {

    @Override   // 添加课程分类
    public void addSubject(MultipartFile file,EduSubjectService service) {


        try {
            // 文件输入流
            InputStream in = file.getInputStream();
            EasyExcel.read(in, ExcelSubjectData.class,new SubjectListen(service)).sheet().doRead();

        }catch (Exception e){

            e.printStackTrace();
        }

    }

    @Override
    public List<SubjectNodeTwo> nestedList() {
        //最终要的到的数据列表
        ArrayList<SubjectNodeTwo> subjectNestedVoArrayList = new ArrayList<>();


        //获取一级分类数据记录

        QueryWrapper<EduSubject> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id", 0);
        queryWrapper.orderByAsc("sort", "id");
        List<EduSubject> subjects = baseMapper.selectList(queryWrapper);
        //获取二级分类数据记录

        QueryWrapper<EduSubject> queryWrapper2 = new QueryWrapper<>();

        queryWrapper2.ne("parent_id", 0);

        queryWrapper2.orderByAsc("sort", "id");

        List<EduSubject> subSubjects = baseMapper.selectList(queryWrapper2);

        //填充一级分类vo数据
        int count = subjects.size();
        for (int i = 0; i < count; i++) {

            EduSubject subject = subjects.get(i);

            //创建一级类别vo对象

            SubjectNodeTwo subjectNodeTwo = new SubjectNodeTwo();

            BeanUtils.copyProperties(subject, subjectNodeTwo);

            subjectNestedVoArrayList.add(subjectNodeTwo);

            //填充二级分类vo数据
            ArrayList<SubjectNode> subjectVoArrayList = new ArrayList<>();

            int count2 = subSubjects.size();

            for (int j = 0; j < count2; j++) {

                EduSubject subSubject = subSubjects.get(j);

                if(subject.getId().equals(subSubject.getParentId())){

                    //创建二级类别vo对象
                    SubjectNode subjectVo = new SubjectNode();

                    BeanUtils.copyProperties(subSubject, subjectVo);

                    subjectVoArrayList.add(subjectVo);

                }

            }

            subjectNodeTwo.setChildren(subjectVoArrayList);

        }

        return subjectNestedVoArrayList;
    }
}
