package com.atshangluo.eduservice.entity.vo;

import java.util.*;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName SubjectNodeTwo.java
 * @Description TODO
 * @createTime 2021年07月27日 10:34:00
 */
public class SubjectNodeTwo {

    private String id;
    private String title;
    private List<SubjectNode> children = new ArrayList<>();

    public SubjectNodeTwo() {
    }

    public SubjectNodeTwo(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SubjectNode> getChildren() {
        return children;
    }

    public void setChildren(List<SubjectNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "SubjectNodeTwo{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
