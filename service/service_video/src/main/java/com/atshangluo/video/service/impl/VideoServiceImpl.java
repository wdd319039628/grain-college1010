package com.atshangluo.video.service.impl;


import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.ram.model.v20150501.DeleteRoleRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoRequest;
import com.aliyuncs.vod.model.v20170321.DeleteVideoResponse;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.atshangluo.video.service.VideoService;
import com.atshangluo.video.utils.ClientUtils;
import org.apache.commons.lang.StringUtils;
import com.atshangluo.video.utils.PropertiesUtils;
import org.springframework.stereotype.Service;

import org.springframework.web.multipart.MultipartFile;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName VideoServiceImpl.java
 * @Description TODO
 * @createTime 2021年07月29日 14:57:00
 */
@Service
@Slf4j
public class VideoServiceImpl implements VideoService {


    @Override
    public String[] uploadVideo(MultipartFile file) {

        String[] info = new String[3];
        try{

            InputStream inputStream = file.getInputStream();

            String originalFilename = file.getOriginalFilename();

            String title = originalFilename.substring(0, originalFilename.lastIndexOf("."));

            info[0] = title;

            UploadStreamRequest request = new UploadStreamRequest(

                    PropertiesUtils.ACCESS_KEY_ID,

                    PropertiesUtils.ACCESS_KEY_SECRET,

                    title, originalFilename, inputStream);


            UploadVideoImpl uploader = new UploadVideoImpl();

            UploadStreamResponse response = uploader.uploadStream(request);


            //如果设置回调URL无效，不影响视频上传，可以返回VideoId同时会返回错误码。
            // 其他情况上传失败时，VideoId为空，此时需要根据返回错误码分析具体错误原因
            String videoId = response.getVideoId();

            info[1] = videoId;

            if (!response.isSuccess()) {

                String errorMessage = "阿里云上传错误：" + "code：" + response.getCode() + ", message：" + response.getMessage();

                log.warn(errorMessage);

                if(StringUtils.isEmpty(videoId)){

                    throw new EduException(20001, errorMessage);

                }

            }
            info[2] = "900";

            return info;



        }catch (Exception e){
            info[2] = "404";
            return info;
        }


    }

    @Override
    public void deleteVideo(String videoId) {

        try{
            DefaultAcsClient client = ClientUtils.initVodClient(PropertiesUtils.ACCESS_KEY_ID,
                                                                 PropertiesUtils.ACCESS_KEY_SECRET);

            DeleteVideoRequest request = new DeleteVideoRequest();

            request.setVideoIds(videoId);

            DeleteVideoResponse response = client.getAcsResponse(request);
        }catch (ClientException e){
            throw new EduException(20001,"删除失败");
        }
    }

    @Override
    public void deleteVideoByCourse(List<String> videoIds) {
        try{
            DefaultAcsClient client = ClientUtils.initVodClient(PropertiesUtils.ACCESS_KEY_ID,
                    PropertiesUtils.ACCESS_KEY_SECRET);

            DeleteVideoRequest request = new DeleteVideoRequest();

            String str = StringUtils.join(videoIds.toArray(),",");

            request.setVideoIds(str);

            DeleteVideoResponse response = client.getAcsResponse(request);
        }catch (ClientException e){
            throw new EduException(20001,"删除失败");
        }
    }
}
