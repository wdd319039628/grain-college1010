package com.atshangluo.educms.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.educms.entity.CrmBanner;
import com.atshangluo.educms.service.CrmBannerService;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 首页banner表 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-07-30
 */
@Api(description = "后台banner管理接口")

@RestController
@RequestMapping("/eduCms/adminBanner")
public class AdminBannerController {

    @Autowired
    private CrmBannerService bannerService;

    @GetMapping("/{page}/{limit}")
    public R getPageBanner(@PathVariable("page") long page,
                           @PathVariable("limit") long limit){

        Page<CrmBanner> bannerPage = new Page<>(page,limit);
        bannerService.page(bannerPage,null);

        return R.ok().data("list",bannerPage.getRecords()).data("total",bannerPage.getTotal());

    }

    @GetMapping("/getBanner/{id}")
    public R  getBanner(@PathVariable("id") String id){

        CrmBanner banner = bannerService.getById(id);

        return R.ok().data("banner",banner);

    }


    @PostMapping("/add")
    public R addBanner(@RequestBody CrmBanner banner){

        bannerService.save(banner);

        return R.ok();
    }

    @PostMapping("/update")
    public R update(@RequestBody CrmBanner banner){

        bannerService.updateById(banner);

        return R.ok();
    }

    @DeleteMapping("/delete/{id}")
    public R delete(@PathVariable("id") String id){

        bannerService.removeById(id);

        return R.ok();
    }

}

