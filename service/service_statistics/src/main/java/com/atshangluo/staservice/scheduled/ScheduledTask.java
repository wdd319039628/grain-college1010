package com.atshangluo.staservice.scheduled;

import com.atshangluo.commonutils.DateUtil;
import com.atshangluo.staservice.service.DailyService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName ScheduledTask.java
 * @Description TODO
 * @createTime 2021年08月02日 16:26:00
 */
@Component
public class ScheduledTask {


    @Autowired
    private DailyService dailyService;


    /**

     * 测试

     * 每天七点到二十三点每五秒执行一次

     */


//    @Scheduled(cron = "0/5 * * * * ?")
//    public void task1() {
//
//        System.out.println("*********++++++++++++*****执行了");
//
//    }


    /**

     * 每天凌晨1点执行定时

     */

    @Scheduled(cron = "0 0 1 * * ?")
    public void task2() {

        //获取上一天的日期

        String day = DateUtil.formatDate(DateUtil.addDays(new Date(), -1));

        dailyService.registerCount(day);


    }

}
