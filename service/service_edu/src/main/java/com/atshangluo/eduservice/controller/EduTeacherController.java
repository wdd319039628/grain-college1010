package com.atshangluo.eduservice.controller;


import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.entity.EduTeacher;
import com.atshangluo.eduservice.entity.vo.TeacherQuery;
import com.atshangluo.eduservice.service.EduTeacherService;

import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author wsm
 * @since 2021-07-24
 */

@Api(description = "讲师管理")
@RestController
@RequestMapping("/eduService/teacher")
public class EduTeacherController {

    @Autowired  // 注入Service
    private EduTeacherService eduTeacherService;

    @ApiOperation(value = "获取所有讲师")
    @GetMapping("/list")
    public R findAllTeacher(){
        List<EduTeacher> list = eduTeacherService.list(null);

        return R.ok().data("list",list);
    }

    @CacheEvict(value = "removeTeacher",key = "'deleteById'",allEntries = true)
    @ApiOperation(value = "根据ID删除讲师")
    @DeleteMapping("/delete/{id}")
    public R removeTeacher(
            @ApiParam(name = "id",value = "讲师id",required = true)
            @PathVariable("id") String id){
        boolean flag = eduTeacherService.removeById(id);
        if (flag){
            return R.ok();
        }else{
            return R.error();
        }
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("{page}/{limit}")
    public R pageList(
            @ApiParam(name = "page",value = "当前页码",required = true)
            @PathVariable("page")
            Long page,
            @ApiParam(name = "limit",value = "每页数量",required = true)
            @PathVariable("limit")
            Long limit){

        Page<EduTeacher> pageParam = new Page<>(page,limit);
        eduTeacherService.page(pageParam,null);
        List<EduTeacher> teachers = pageParam.getRecords();
        long total = pageParam.getTotal();
        return R.ok().data("total",total).data("items",teachers);
    }

    @ApiOperation(value = "分页查询带条件")
    @PostMapping("pageTeacherCondition/{page}/{limit}")
    public R pageTeacherCondition(
            @ApiParam(name = "page",value = "当前页码",required = true)
            @PathVariable("page") Long page,
            @ApiParam(name = "limit",value = "每页数量",required = true)
            @PathVariable("limit")
                    Long limit,
            // requestBody需要Post才能接受到数据
            @RequestBody(required = false) TeacherQuery teacherQuery){

        Page<EduTeacher> pageParam = new Page<>(page,limit);
        String name = teacherQuery.getName();
        Integer level = teacherQuery.getLevel();
        String begin = teacherQuery.getBegin();
        String end = teacherQuery.getEnd();
        QueryWrapper<EduTeacher> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(name)){
            wrapper.like("name",name);
        }
        if (!StringUtils.isEmpty(level)){
            wrapper.eq("level",level);
        }
        if (!StringUtils.isEmpty(begin)){
            wrapper.ge("gmt_create", begin);
        }
        if (!StringUtils.isEmpty(end)){
            wrapper.le("gmt_modified", end);
        }
        // 排序
        wrapper.orderByDesc("gmt_create");
        eduTeacherService.page(pageParam,wrapper);
        List<EduTeacher> teachers = pageParam.getRecords();
        long total = pageParam.getTotal();
        return R.ok().data("total",total).data("items",teachers);
    }


    @CacheEvict(value = "addTeacher",key = "'backView'",allEntries = true)
    @ApiOperation(value = "添加讲师")
    @PostMapping("/addTeacher")
    public R add(
            @ApiParam(name = "teacher",value = "讲师对象",required = true)
            @RequestBody EduTeacher teacher){

        boolean flag = eduTeacherService.save(teacher);
        if (flag){
            return R.ok();
        }else{
            return R.error();
        }

    }


    @ApiOperation(value = "根据Id查询讲师")
    @GetMapping("/getTeacher/{id}")
    public R getTeacherById(
            @ApiParam(name = "id",value = "讲师Id",required = true)
            @PathVariable("id")String id){

        EduTeacher teacher = eduTeacherService.getById(id);
        return R.ok().data("teacher",teacher);

    }

    @ApiOperation(value = "修改讲师")
    @PostMapping("/updateTeacher")
    public R updateTeacher(
            @ApiParam(name = "teacher",value = "讲师",required = true)
            @RequestBody EduTeacher teacher){

        boolean flag = eduTeacherService.updateById(teacher);
        if (flag){
            return R.ok();
        }else{
            return R.error();
        }

    }






}

