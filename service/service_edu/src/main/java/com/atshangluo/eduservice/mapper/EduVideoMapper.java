package com.atshangluo.eduservice.mapper;

import com.atshangluo.eduservice.entity.EduVideo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 课程视频 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
public interface EduVideoMapper extends BaseMapper<EduVideo> {

    List getCourseIdList(String courseId);
}
