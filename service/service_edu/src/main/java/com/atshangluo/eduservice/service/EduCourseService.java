package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduCourse;
import com.atshangluo.eduservice.entity.vo.*;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.HashMap;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
public interface EduCourseService extends IService<EduCourse> {

    String addCourse(CourseInfoForm info);

    CourseInfoForm getCourseById(String courseId);

    void updateCourse(CourseInfoForm courseInfo);

    CoursePublish getCoursePublishInfo(String courseId);

    void pageQuery(Page<EduCourse> param, CourseQuery query);

    boolean removeCourseById(String courseId);

    HashMap<String, Object> getCourseFront(Page<EduCourse> param, CourseQueryVo queryVo);



    CourseInfoVo selectInfoWebById(String id);


    void updatePageViewCount(String id);
}
