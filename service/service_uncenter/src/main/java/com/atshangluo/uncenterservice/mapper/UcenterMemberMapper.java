package com.atshangluo.uncenterservice.mapper;

import com.atshangluo.uncenterservice.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author wsm
 * @since 2021-07-31
 */
@Repository
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {



    Integer countRegisterDay(String day);
}
