package com.atshangluo.eduservice.service.impl;

import com.atshangluo.commonutils.R;
import com.atshangluo.eduservice.client.VideoClient;
import com.atshangluo.eduservice.entity.EduVideo;
import com.atshangluo.eduservice.mapper.EduVideoMapper;
import com.atshangluo.eduservice.service.EduVideoService;
import com.atshangluo.servicebase.exceptionHandler.EduException;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 课程视频 服务实现类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
@Service
public class EduVideoServiceImpl extends ServiceImpl<EduVideoMapper, EduVideo> implements EduVideoService {

    @Autowired
    private VideoClient videoClient;


    @Override
    public boolean removeVideoById(String courseId) {
        QueryWrapper<EduVideo> wrapper = new QueryWrapper<>();

        wrapper.eq("course_id",courseId);


        List<EduVideo> videos = baseMapper.selectList(wrapper);

        List<String> videoIds = new ArrayList<>();

        if (videos.size()>0){
            for (EduVideo video : videos) {
                if (!StringUtils.isEmpty(video.getVideoSourceId())){

                    videoIds.add(video.getVideoSourceId());

                }
            }

        }
        if (videoIds.size()>0){

            R result = videoClient.deleteByCourse(videoIds);
            if (result.getCode()==20001){
                throw new EduException(20001,"删除视频失败，熔断器。。。。");
            }
        }



        int count = baseMapper.selectCount(wrapper);
        int i = baseMapper.delete(wrapper);




        if (i==count){
            return true;
        }else {

            return false;
        }

    }
}
