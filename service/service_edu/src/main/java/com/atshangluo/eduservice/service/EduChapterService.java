package com.atshangluo.eduservice.service;

import com.atshangluo.eduservice.entity.EduChapter;
import com.atshangluo.eduservice.entity.chapter.BigChapter;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author wsm
 * @since 2021-07-27
 */
public interface EduChapterService extends IService<EduChapter> {

    List<BigChapter> getChapter(String id);

    String addChapter(EduChapter chapter);

    boolean removeChapterById(String chapterId);

    boolean removeChapterByCourseId(String courseId);
}
