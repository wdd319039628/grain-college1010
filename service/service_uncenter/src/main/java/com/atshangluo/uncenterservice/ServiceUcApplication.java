package com.atshangluo.uncenterservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author admin
 * @version 1.0.0
 * @ClassName ServiceUcApplication.java
 * @Description TODO
 * @createTime 2021年07月31日 09:55:00
 */
@ComponentScan({"com.atshangluo"})
@SpringBootApplication//取消数据源自动配置
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.atshangluo.uncenterservice.mapper")
public class ServiceUcApplication {

    public static void main(String[] args) {

        SpringApplication.run(ServiceUcApplication.class, args);

    }

}
